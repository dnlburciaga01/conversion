const btnCalcular = document.getElementById('btnCalcular');

//Evento change para las opciones del selectS
monedaOrigen.addEventListener("change", () => {
    const opciones = monedaDestino.options;
    const monedaSeleccionada = monedaOrigen.value;

    for (let i = 0; i < opciones.length; i++) {
        if (opciones[i].value === monedaSeleccionada) {
            opciones[i].disabled = true;                    // deshabilitar opción
            opciones[i].selected = false;                   // deseleccionar opción
            opciones[i].style.display = "none";             // ocultar opción
        } else {
            opciones[i].disabled = false;                   // habilitar otras opciones
            opciones[i].style.display = "block";            // mostrar opciones
        }
    }
});

btnCalcular.addEventListener("click", function(){

    //obtener los valores de los inputs text
    const cantidad = parseFloat(document.getElementById("cantidad").value);
    const monedaOrigen = parseFloat(document.getElementById("monedaOrigen").value);
    const monedaDestino = parseFloat(document.getElementById("monedaDestino").value);
  
    //Tabla de conversion
    const tasaUSD = 1;
    const tasaMXN = 19.85;
    const tasaCAN = 1.35;
    const tasaEUR = 0.99;
    
    let subtotal;

      switch (monedaOrigen) {
        case 1:                                             // Dolar Estadounidense
          switch (monedaDestino) {
            case 1: 
              subtotal = cantidad * tasaUSD;
              break;
            case 2: 
              subtotal = cantidad * tasaMXN;
              break;
            case 3: 
              subtotal = cantidad * tasaCAN;
              break;
            case 4:
              subtotal = cantidad * tasaEUR;
              break;
          }
          break;
        case 2:                                             // Peso Mexicano
          switch (monedaDestino) {
            case 1: 
              subtotal = cantidad / tasaMXN;
              break;
            case 2: 
              subtotal = cantidad * tasaUSD / tasaMXN;
              break;
            case 3: 
              subtotal = cantidad / tasaMXN * tasaCAN;
              break;
            case 4: 
              subtotal = cantidad / tasaMXN * tasaEUR;
              break;
          }
          break;
        case 3:                                             // Dolar Canadiense
          switch (monedaDestino) {
            case 1: 
              subtotal = cantidad / tasaCAN;
              break;
            case 2: 
              subtotal = cantidad / tasaCAN * tasaMXN;
              break;
            case 3: 
              subtotal = cantidad * tasaUSD;
              break;
            case 4:
              subtotal = cantidad / tasaCAN * tasaEUR;
              break;
          }
          break;
        case 4:                                             // Euro
          switch (monedaDestino) {
            case 1: 
              subtotal = cantidad / tasaEUR;
              break;
            case 2: 
              subtotal = cantidad / tasaEUR * tasaMXN;
              break;
            case 3: 
              subtotal = cantidad / tasaEUR * tasaCAN;
              break;
            case 4: 
              subtotal = cantidad * tasaUSD / tasaEUR;
            break;
            }
    break;

    }
    const tComision = subtotal * 0.03;
    const tPagar = subtotal + tComision;

    document.getElementById('subtotal').value = subtotal.toFixed(2)
    document.getElementById('tComision').value = tComision.toFixed(2);
    document.getElementById('tPagar').value = tPagar.toFixed(2);
});

//Codificación del botón Registrar
function agregarRegistro() {
    const cantidad = parseInt(document.getElementById("cantidad").value);
    const monedaOrigen = parseInt(document.getElementById("monedaOrigen").value);
    const monedaDestino = parseInt(document.getElementById("monedaDestino").value);
    const tPagar = parseFloat(document.getElementById("tPagar").value);

    let origenTxt;
    let destinoTxt;

    switch(monedaOrigen){
        case 1:
            origenTxt="Dolar Estadounidense a";
            break;
        case 2:
            origenTxt="Pesos Mexicanos a";
            break;
        case 3:
            origenTxt="Dolar Canadiense a";
            break;
        case 4:
            origenTxt="Euro a";
            break;
    }
    switch(monedaDestino){
        case 1:
            destinoTxt="Dolar Estadounidense";
            break;
        case 2:
            destinoTxt="Pesos Mexicanos";
            break;
        case 3:
            destinoTxt="Dolar Canadiense";
            break;
        case 4:
            destinoTxt="Euro";
            break;
    }
    const tabla = document.getElementById("registros");
    const newRow = `
    <tr>
      <td>${cantidad}</td>
      <td>${origenTxt}</td>
      <td>${destinoTxt}</td>
      <td>${tPagar.toFixed(2)}</td>
    </tr>
  `;
  
    tabla.innerHTML += newRow;

    let tGeneral = 0;
    const filas = tabla.getElementsByTagName("tr");
  
    for (let i = 1; i < filas.length; i++) {
      const tPagar = parseFloat(filas[i].getElementsByTagName("td")[3].textContent);
      tGeneral += tPagar;
    }
  
    document.getElementById("total").textContent = "$"+tGeneral.toFixed(2);
  }

//Botón Borrar
function eliminarRegistro(){
    const tabla = document.getElementById("registros");
    let numRows = tabla.rows.length;
    for (let i = numRows - 1; i > 0; i--) {
        tabla.deleteRow(i);
    }
    let totalGeneral=0;
    document.getElementById("total").textContent = totalGeneral;
  }
